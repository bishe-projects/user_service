package main

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/user"
	"gitlab.com/bishe-projects/user_service/src/interface/facade"
)

// UserServiceImpl implements the last service interface defined in the IDL.
type UserServiceImpl struct{}

// UserLogin implements the UserServiceImpl interface.
func (s *UserServiceImpl) UserLogin(ctx context.Context, req *user.UserLoginReq) (resp *user.UserLoginResp, err error) {
	klog.CtxInfof(ctx, "UserLogin req=%+v", req)
	resp = facade.UserFacade.UserLogin(ctx, req)
	klog.CtxInfof(ctx, "UserLogin resp=%+v", resp)
	return
}

// UserRegister implements the UserServiceImpl interface.
func (s *UserServiceImpl) UserRegister(ctx context.Context, req *user.UserRegisterReq) (resp *user.UserRegisterResp, err error) {
	klog.CtxInfof(ctx, "UserRegister req=%+v", req)
	resp = facade.UserFacade.UserRegister(ctx, req)
	klog.CtxInfof(ctx, "UserRegister resp=%+v", resp)
	return
}

// GetUserByID implements the UserServiceImpl interface.
func (s *UserServiceImpl) GetUserByID(ctx context.Context, req *user.GetUserByIDReq) (resp *user.GetUserByIDResp, err error) {
	klog.CtxInfof(ctx, "GetUserByID req=%+v", req)
	resp = facade.UserFacade.GetUserByUserID(ctx, req)
	klog.CtxInfof(ctx, "GetUserByID resp=%+v", resp)
	return
}

// GetUserByUsername implements the UserServiceImpl interface.
func (s *UserServiceImpl) GetUserByUsername(ctx context.Context, req *user.GetUserByUsernameReq) (resp *user.GetUserByUsernameResp, err error) {
	klog.CtxInfof(ctx, "GetUserByUsername req=%+v", req)
	resp = facade.UserFacade.GetUserByUsername(ctx, req)
	klog.CtxInfof(ctx, "GetUserByUsername resp=%+v", resp)
	return
}

// ChangePassword implements the UserServiceImpl interface.
func (s *UserServiceImpl) ChangePassword(ctx context.Context, req *user.ChangePasswordReq) (resp *user.ChangePasswordResp, err error) {
	klog.CtxInfof(ctx, "ChangePassword req=%+v", req)
	resp = facade.UserFacade.ChangePassword(ctx, req)
	klog.CtxInfof(ctx, "ChangePassword resp=%+v", resp)
	return
}

// FunctionAdminList implements the UserServiceImpl interface.
func (s *UserServiceImpl) FunctionAdminList(ctx context.Context, req *user.FunctionAdminListReq) (resp *user.FunctionAdminListResp, err error) {
	klog.CtxInfof(ctx, "FunctionAdminList req=%+v", req)
	resp = facade.UserFacade.FunctionAdminList(ctx, req)
	klog.CtxInfof(ctx, "FunctionAdminList resp=%+v", resp)
	return
}

// AllRoleList implements the UserServiceImpl interface.
func (s *UserServiceImpl) AllRoleList(ctx context.Context, req *user.AllRoleListReq) (resp *user.AllRoleListResp, err error) {
	klog.CtxInfof(ctx, "AllRoleList req=%+v", req)
	resp = facade.RoleFacade.AllRoleList(ctx, req)
	klog.CtxInfof(ctx, "AllRoleList resp=%+v", resp)
	return
}

// UserRoleList implements the UserServiceImpl interface.
func (s *UserServiceImpl) UserRoleList(ctx context.Context, req *user.UserRoleListReq) (resp *user.UserRoleListResp, err error) {
	klog.CtxInfof(ctx, "UserRoleList req=%+v", req)
	resp = facade.RoleFacade.UserRoleList(ctx, req)
	klog.CtxInfof(ctx, "UserRoleList resp=%+v", resp)
	return
}

// UserAddRole implements the UserServiceImpl interface.
func (s *UserServiceImpl) UserAddRole(ctx context.Context, req *user.UserAddRoleReq) (resp *user.UserAddRoleResp, err error) {
	klog.CtxInfof(ctx, "UserAddRole req=%+v", req)
	resp = facade.UserRoleFacade.UserAddRole(ctx, req)
	klog.CtxInfof(ctx, "UserAddRole resp=%+v", resp)
	return
}

// UserRemoveRole implements the UserServiceImpl interface.
func (s *UserServiceImpl) UserRemoveRole(ctx context.Context, req *user.UserRemoveRoleReq) (resp *user.UserRemoveRoleResp, err error) {
	klog.CtxInfof(ctx, "UserRemoveRole req=%+v", req)
	resp = facade.UserRoleFacade.UserRemoveRole(ctx, req)
	klog.CtxInfof(ctx, "UserRemoveRole resp=%+v", resp)
	return
}

// HasUserRole implements the UserServiceImpl interface.
func (s *UserServiceImpl) HasUserRole(ctx context.Context, req *user.HasUserRoleReq) (resp *user.HasUserRoleResp, err error) {
	klog.CtxInfof(ctx, "HasUserRole req=%+v", req)
	resp = facade.UserRoleFacade.HasUserRole(ctx, req)
	klog.CtxInfof(ctx, "HasUserRole resp=%+v", resp)
	return
}
