package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/user_service/src/domain/user/entity"
	"gitlab.com/bishe-projects/user_service/src/domain/user/service"
)

var RoleApp = new(Role)

type Role struct {}

func (a *Role) AllRoleList() ([]*entity.Role, *business_error.BusinessError) {
	return service.RoleDomain.AllRoleList()
}

func (a *Role) UserRoleList(userID int64) ([]*entity.Role, *business_error.BusinessError) {
	return service.RoleDomain.UserRoleList(userID)
}