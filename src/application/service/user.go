package service

import (
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/user_service/src/domain/user/entity"
	"gitlab.com/bishe-projects/user_service/src/domain/user/service"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/user_error"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/util"
)

var UserApp = new(User)

type User struct{}

func (a *User) Register(registerUser *entity.User) *business_error.BusinessError {
	existUser, err := service.UserDomain.GetUserByUsername(registerUser.Name)
	if err != nil {
		return err
	}
	if existUser != nil {
		return user_error.UserRegisterUsernameExisted
	}
	err = service.UserDomain.CreateUser(registerUser)
	if err != nil {
		return err
	}
	return nil
}

func (a *User) Login(loginUser *entity.User) (*entity.User, string, *business_error.BusinessError) {
	user, err := service.UserDomain.GetUserByUsername(loginUser.Name)
	if err != nil {
		return nil, "", err
	}
	if user == nil {
		return nil, "", user_error.UserNotExistsErr
	}
	if !user.IsInputPasswordValid(loginUser.Password) {
		return nil, "", user_error.UserLoginErr
	}
	token, tokenErr := common_utils.TokenSign(map[string]interface{}{"uid": user.ID})
	if tokenErr != nil {
		klog.Errorf("[AppUserService-Login] TokenSign failed: uid=%d err=%s", user.ID, err)
		return nil, "", user_error.TokenSignErr
	}
	return user, token, nil
}

func (a *User) GetUserByID(userID int64) (*entity.User, *business_error.BusinessError) {
	return service.UserDomain.GetUserByID(userID)
}

func (a *User) GetUserByUsername(username string) (*entity.User, *business_error.BusinessError) {
	return service.UserDomain.GetUserByUsername(username)
}

func (a *User) ChangePassword(changeUser *entity.User, newPassword string) *business_error.BusinessError {
	existUser, err := service.UserDomain.GetUserByID(changeUser.ID)
	if err != nil {
		return err
	}
	if existUser == nil {
		return user_error.UserNotExistsErr
	}
	klog.Infof("password=%s input=%s check=%v", existUser.Password, changeUser.Password, existUser.IsInputPasswordValid(changeUser.Password))
	if !existUser.IsInputPasswordValid(changeUser.Password) {
		return user_error.UserChangePasswordErr
	}
	existUser.Password = newPassword
	if !existUser.CheckPasswordRules() {
		return user_error.UserPasswordNotSatisfied
	}
	return service.UserDomain.UpdateUser(existUser, map[string]interface{}{"password": util.GetMD5Hash(newPassword)})
}

func (a *User) FunctionAdminList(functionId int64) ([]*entity.User, *business_error.BusinessError) {
	return service.UserDomain.FunctionAdminList(functionId)
}
