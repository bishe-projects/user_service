package service

import (
	"context"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/user_service/src/domain/user_role/entity"
	"gitlab.com/bishe-projects/user_service/src/domain/user_role/service"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/loaders"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/user_error"
)

var UserRoleApp = new(UserRole)

type UserRole struct{}

func (a *UserRole) UserAddRole(ctx context.Context, userRole *entity.UserRole) *business_error.BusinessError {
	loader := loaders.NewGetFunctionByIDLoader(ctx, userRole.FunctionId)
	if err := loader.Load(); err != nil {
		return user_error.GetFunctionByIDErr
	}
	if loader.Function == nil {
		return user_error.FunctionNotFoundErr
	}
	businessErr := service.UserRoleDomain.UserAddRole(userRole)
	if businessErr == business_error.DataExistsErr {
		return user_error.UserAlreadyHasRole
	}
	return businessErr
}

func (a *UserRole) UserRemoveRole(uid, roleID, functionID int64) *business_error.BusinessError {
	userRole, err := service.UserRoleDomain.GetUserRole(uid, roleID, &functionID)
	if err != nil {
		return user_error.GetUserRoleErr
	}
	if userRole == nil {
		return user_error.UserRoleNotExistsErr
	}
	return service.UserRoleDomain.UserRemoveRole(userRole)
}

func (a *UserRole) HasUserRole(uid, roleID int64, functionID *int64) (bool, *business_error.BusinessError) {
	userRole, err := service.UserRoleDomain.GetUserRole(uid, roleID, functionID)
	if err != nil {
		return false, err
	}
	return userRole != nil, nil
}
