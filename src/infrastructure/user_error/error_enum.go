package user_error

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
)

// internal server error
var (
	UserNotExistsErr     = business_error.NewBusinessError("user not exists", -10001)
	CreateUserErr        = business_error.NewBusinessError("create user failed", -10002)
	GetUserByNameErr     = business_error.NewBusinessError("get user by name failed", -10003)
	GetUserByIDErr       = business_error.NewBusinessError("get user by id failed", -10004)
	GetFunctionByIDErr   = business_error.NewBusinessError("get function by id failed", -10005)
	UserUpdateErr        = business_error.NewBusinessError("user update failed", -10006)
	TokenSignErr         = business_error.NewBusinessError("token sign failed", -10007)
	GetAllRoleListErr    = business_error.NewBusinessError("get all role list failed", -10008)
	GetUserRoleListErr   = business_error.NewBusinessError("get user role list failed", -10009)
	GetUserRoleErr       = business_error.NewBusinessError("get user role failed", -10011)
	UserAddRoleErr       = business_error.NewBusinessError("user add role failed", -10012)
	UserRemoveRoleErr    = business_error.NewBusinessError("user remove role failed", -10013)
	UserRoleNotExistsErr = business_error.NewBusinessError("user role not exists", -10014)
	FunctionAdminListErr = business_error.NewBusinessError("get function admin list failed", -10015)
	FunctionNotFoundErr  = business_error.NewBusinessError("function not found", -10016)
)

// logic error
var (
	UserRegisterUsernameExisted   = business_error.NewBusinessError("username already existed", -30001)
	UserRegisterPasswordNotEqual  = business_error.NewBusinessError("register user password and confirm password not equal", -30002)
	UserLoginErr                  = business_error.NewBusinessError("username or password incorrect", -30003)
	UserPasswordNotSatisfied      = business_error.NewBusinessError("user password is not satisfied", -30004)
	UserChangePasswordErr         = business_error.NewBusinessError("user old password incorrect", -30005)
	UserChangeNewPasswordNotEqual = business_error.NewBusinessError("user new password and confirm password not equal", -30006)
	UserAlreadyHasRole            = business_error.NewBusinessError("this user already has this role", -30006)
)
