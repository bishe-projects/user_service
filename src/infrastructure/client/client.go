package client

import (
	"github.com/cloudwego/kitex/client"
	"github.com/cloudwego/kitex/transport"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/function/functionservice"
)

var (
	FunctionClient = functionservice.MustNewClient("function_service", client.WithHostPorts("0.0.0.0:8880"), client.WithTransportProtocol(transport.TTHeaderFramed))
)
