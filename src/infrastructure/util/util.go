package util

import (
	"crypto/md5"
	"encoding/hex"
)

func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

//func GetUserIDFromToken(ctx context.Context, token string) (int64, *business_error.BusinessError) {
//	claims, err := common_utils.TokenParse(token)
//	if claims == nil || err != nil {
//		klog.CtxErrorf(ctx, "[GetUserIDFromToken] token parse failed: token=%s err=%s", token, err)
//		return 0, user_error.TokenParseErr
//	}
//	klog.CtxInfof(ctx, "claims=%+v", claims)
//	uid, ok := claims["uid"]
//	if !ok {
//		klog.CtxErrorf(ctx, "[GetUserIDFromToken] token doesn't contain uid field: claims=%+v token=%s", claims, token)
//		return 0, user_error.TokenParseErr
//	}
//	klog.CtxInfof(ctx, "uid=%+v type=%+v", uid, reflect.TypeOf(uid).String())
//	id, ok := uid.(float64)
//	if !ok {
//		klog.CtxErrorf(ctx, "[GetUserIDFromToken] uid parsed to int64 failed: uid=%v claims=%+v token=%s", uid, claims, token)
//		return 0, user_error.TokenParseErr
//	}
//	return int64(id), nil
//}
