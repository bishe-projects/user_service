package loaders

import (
	"context"
	"errors"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/loader"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/function"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/client"
)

type GetFunctionByIDLoader struct {
	loader.CommonLoader
	// req
	ctx        context.Context
	functionID int64
	// resp
	Function *function.Function
}

func (l *GetFunctionByIDLoader) Load() error {
	resp, err := client.FunctionClient.GetFunctionByID(l.ctx, l.newGetFunctionByIDReq())
	if err == nil && resp.BaseResp != nil && resp.BaseResp.Status < 0 {
		err = errors.New(resp.BaseResp.Message)
	}
	if err != nil {
		klog.CtxErrorf(l.ctx, "[GetFunctionByIDLoader] get function by id failed: functionID=%d, err=%s", l.functionID, err)
		l.SetError(err)
		return err
	}
	l.Function = resp.Function
	return nil
}

func (l *GetFunctionByIDLoader) newGetFunctionByIDReq() *function.GetFunctionByIDReq {
	return &function.GetFunctionByIDReq{
		FunctionId: l.functionID,
	}
}

func NewGetFunctionByIDLoader(ctx context.Context, functionID int64) *GetFunctionByIDLoader {
	return &GetFunctionByIDLoader{
		ctx:        ctx,
		functionID: functionID,
	}
}
