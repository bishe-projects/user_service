package repo

import (
	"gitlab.com/bishe-projects/user_service/src/infrastructure/repo/mysql"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/repo/po"
)

type UserRoleDao interface {
	GetUserRole(int64, int64, *int64) (*po.UserRole, error)
	UserAddRole(*po.UserRole) error
	UserRemoveRole(*po.UserRole) error
}

var UserRoleRepo = NewUserRoleRepo(mysql.UserRoleMySQLDao)

type UserRole struct {
	UserRoleDao UserRoleDao
}

func NewUserRoleRepo(userRoleDao UserRoleDao) *UserRole {
	return &UserRole{
		UserRoleDao: userRoleDao,
	}
}
