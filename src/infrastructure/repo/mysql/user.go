package mysql

import (
	"gitlab.com/bishe-projects/user_service/src/infrastructure/repo/po"
)

var (
	UserMySQLDao = new(User)
)

type User struct{}

const userTable = "user"

func (r *User) CreateUser(user *po.User) error {
	return db.Table(userTable).Create(&user).Error
}

func (r *User) GetUserByID(userID int64) (*po.User, error) {
	var user *po.User
	result := db.Table(userTable).First(&user, userID)
	return user, result.Error
}

func (r *User) GetUserByUsername(username string) (*po.User, error) {
	var user *po.User
	result := db.Table(userTable).Where("name = ?", username).First(&user)
	return user, result.Error
}

func (r *User) UpdateUser(user *po.User, values map[string]interface{}) error {
	return db.Table(userTable).Model(user).Updates(values).Error
}

func (r *User) FunctionAdminList(functionId int64) ([]*po.User, error) {
	var adminList []*po.User
	result := db.Table(userRoleTable).Select("user.id, user.name").Joins("join user on user_role.uid = user.id").Where("user_role.function_id = ?", functionId).Scan(&adminList)
	return adminList, result.Error
}
