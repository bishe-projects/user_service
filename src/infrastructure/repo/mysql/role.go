package mysql

import (
	"gitlab.com/bishe-projects/user_service/src/infrastructure/repo/po"
)

var (
	RoleMySQLDao = new(Role)
)

type Role struct{}

const (
	roleTable = "role"
)

func (r *Role) AllRoleList() ([]*po.Role, error) {
	var roleList []*po.Role
	result := db.Table(roleTable).Find(&roleList)
	return roleList, result.Error
}

func (r *Role) UserRoleList(userID int64) ([]*po.RoleResult, error) {
	var roleList []*po.RoleResult
	result := db.Table(userRoleTable).Select("user_role.role_id as id, role.name, function.id as manage_function_id, function.name as manage_function_name").Where("user_role.uid = ?", userID).Joins("join role on user_role.role_id = role.id").Joins("left join `function` on user_role.function_id = function.id").Scan(&roleList)
	return roleList, result.Error
}
