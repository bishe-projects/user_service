package mysql

import (
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/common_utils/mysql"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/repo/po"
)

var (
	UserRoleMySQLDao = new(UserRole)
)

type UserRole struct{}

const (
	userRoleTable = "user_role"
)

func (r *UserRole) GetUserRole(uid, roleID int64, functionID *int64) (*po.UserRole, error) {
	var userRole *po.UserRole
	d := db
	d = d.Table(userRoleTable).Where("uid = ? AND role_id = ?", uid, roleID)
	if functionID != nil {
		d = d.Where("function_id = ?", *functionID)
	}
	result := d.First(&userRole)
	return userRole, result.Error
}

func (r *UserRole) UserAddRole(userRole *po.UserRole) error {
	err := db.Table(userRoleTable).Create(&userRole).Error
	if err != nil {
		if code := mysql.MySqlErrCode(err); code == mysql.ErrDuplicateEntryCode {
			return error_enum.ErrDuplicateEntry
		}
		return err
	}
	return nil
}

func (r *UserRole) UserRemoveRole(userRole *po.UserRole) error {
	return db.Table(userRoleTable).Delete(&userRole).Error
}
