package po

type Role struct {
	ID                 int64
	Name               string
}

type RoleResult struct {
	ID                 int64
	Name               string
	ManageFunctionId   *int64
	ManageFunctionName *string
}