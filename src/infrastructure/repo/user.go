package repo

import (
	"gitlab.com/bishe-projects/user_service/src/infrastructure/repo/mysql"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/repo/po"
)

type UserDao interface {
	CreateUser(*po.User) error
	GetUserByID(int64) (*po.User, error)
	GetUserByUsername(string) (*po.User, error)
	UpdateUser(*po.User, map[string]interface{}) error
	FunctionAdminList(int64) ([]*po.User, error)
}

var UserMySQLRepo = NewUserRepo(mysql.UserMySQLDao)

type User struct {
	UserDao UserDao
}

func NewUserRepo(userDao UserDao) *User {
	return &User{
		UserDao: userDao,
	}
}
