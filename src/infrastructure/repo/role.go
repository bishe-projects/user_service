package repo

import (
	"gitlab.com/bishe-projects/user_service/src/infrastructure/repo/mysql"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/repo/po"
)

type RoleDao interface {
	AllRoleList() ([]*po.Role, error)
	UserRoleList(int64) ([]*po.RoleResult, error)
}

var RoleRepo = NewRoleRepo(mysql.RoleMySQLDao)

type Role struct {
	RoleDao RoleDao
}

func NewRoleRepo(roleDao RoleDao) *Role {
	return &Role {
		RoleDao: roleDao,
	}
}