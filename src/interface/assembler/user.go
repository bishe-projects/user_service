package assembler

import (
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/user"
	"gitlab.com/bishe-projects/user_service/src/domain/user/entity"
)

func ConvertUserRegisterReqToUserEntity(req *user.UserRegisterReq) *entity.User {
	return &entity.User{
		Name:     req.Username,
		Password: req.Password,
	}
}

func ConvertUserLoginReqToUserEntity(req *user.UserLoginReq) *entity.User {
	return &entity.User{
		Name:     req.Username,
		Password: req.Password,
	}
}

func ConvertChangePasswordReqToUserEntity(uid int64, req *user.ChangePasswordReq) *entity.User {
	return &entity.User{
		ID:       uid,
		Password: req.Password,
	}
}

func ConvertUserEntityToUser(entity *entity.User) *user.User {
	return &user.User{
		Uid:      entity.ID,
		Username: entity.Name,
		Roles:    ConvertRoleEntityListToRoleList(entity.Roles),
	}
}

func ConvertUserEntityListToUserList(userEntityList []*entity.User) []*user.User {
	userList := make([]*user.User, 0, len(userEntityList))
	for _, userEntity := range userEntityList {
		userList = append(userList, ConvertUserEntityToUser(userEntity))
	}
	return userList
}
