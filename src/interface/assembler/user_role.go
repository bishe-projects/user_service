package assembler

import (
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/user"
	"gitlab.com/bishe-projects/user_service/src/domain/user_role/entity"
)

func ConvertUserAddRoleReqToUserRoleEntity(req *user.UserAddRoleReq) *entity.UserRole {
	return &entity.UserRole{
		Uid:        req.Uid,
		RoleId:     req.RoleId,
		FunctionId: req.FunctionId,
	}
}
