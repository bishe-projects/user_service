package assembler

import (
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/user"
	"gitlab.com/bishe-projects/user_service/src/domain/user/entity"
)

func ConvertRoleEntityToRole(role *entity.Role) *user.Role {
	return &user.Role{
		Id:                 role.ID,
		Name:               role.Name,
		ManageFunctionId:   role.ManageFunctionId,
		ManageFunctionName: role.ManageFunctionName,
	}
}

func ConvertRoleEntityListToRoleList(roleEntityList []*entity.Role) []*user.Role {
	roleList := make([]*user.Role, 0, len(roleEntityList))
	for _, role := range roleEntityList {
		roleList = append(roleList, ConvertRoleEntityToRole(role))
	}
	return roleList
}
