package facade

import (
	"context"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/user"
	"reflect"
	"testing"
)

func TestUser_GetUserByUsername(t *testing.T) {
	type args struct {
		ctx context.Context
		req *user.GetUserByUsernameReq
	}
	tests := []struct {
		name string
		args args
		want *user.GetUserByUsernameResp
	}{
		// TODO: Add test cases.
		{
			name: "found",
			args: args{
				ctx: context.Background(),
				req: &user.GetUserByUsernameReq{Username: "user0"}},
			want: &user.GetUserByUsernameResp{
				User: &user.User{
					Uid:      2,
					Username: "user0",
				},
			},
		},
		{
			name: "not found",
			args: args{
				ctx: context.Background(),
				req: &user.GetUserByUsernameReq{Username: "-1"}},
			want: &user.GetUserByUsernameResp{
				User: nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &User{}
			got := f.GetUserByUsername(tt.args.ctx, tt.args.req)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetUserByUsername() = %v, want %v", got, tt.want)
			}
			t.Logf("resp=%+v", got)
		})
	}
}
