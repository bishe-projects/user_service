package facade

import (
	"context"
	"github.com/bytedance/gopkg/cloud/metainfo"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/user"
	"gitlab.com/bishe-projects/user_service/src/application/service"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/user_error"
	"gitlab.com/bishe-projects/user_service/src/interface/assembler"
)

var UserFacade = new(User)

type User struct{}

func (f *User) UserRegister(ctx context.Context, req *user.UserRegisterReq) *user.UserRegisterResp {
	resp := &user.UserRegisterResp{}
	if req.Password != req.ConfirmPassword {
		resp.BaseResp = business_error.BaseResp(user_error.UserRegisterPasswordNotEqual)
		return resp
	}
	err := service.UserApp.Register(assembler.ConvertUserRegisterReqToUserEntity(req))
	if err != nil {
		klog.CtxErrorf(ctx, "[UserFacade] user register failed: err=%s", err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	return resp
}

func (f *User) UserLogin(ctx context.Context, req *user.UserLoginReq) *user.UserLoginResp {
	resp := &user.UserLoginResp{}
	userEntity, token, err := service.UserApp.Login(assembler.ConvertUserLoginReqToUserEntity(req))
	if err != nil {
		klog.CtxErrorf(ctx, "[UserFacade] user login failed: err=%s", err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.User = assembler.ConvertUserEntityToUser(userEntity)
	resp.Token = &token
	return resp
}

func (f *User) GetUserByUserID(ctx context.Context, req *user.GetUserByIDReq) *user.GetUserByIDResp {
	resp := &user.GetUserByIDResp{}
	resultUser, err := service.UserApp.GetUserByID(req.Uid)
	if err != nil {
		klog.CtxErrorf(ctx, "[UserFacade] get user by user id failed: err=%s req=%+v", err, req)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	if resultUser != nil {
		resp.User = assembler.ConvertUserEntityToUser(resultUser)
	}
	return resp
}

func (f *User) GetUserByUsername(ctx context.Context, req *user.GetUserByUsernameReq) *user.GetUserByUsernameResp {
	resp := &user.GetUserByUsernameResp{}
	resultUser, err := service.UserApp.GetUserByUsername(req.Username)
	if err != nil {
		klog.CtxErrorf(ctx, "[UserFacade] get user by username failed: req=%+v err=%s ", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	if resultUser != nil {
		resp.User = assembler.ConvertUserEntityToUser(resultUser)
	}
	return resp
}

func (f *User) ChangePassword(ctx context.Context, req *user.ChangePasswordReq) *user.ChangePasswordResp {
	resp := &user.ChangePasswordResp{}
	token, ok := metainfo.GetPersistentValue(ctx, "token")
	if !ok {
		klog.CtxErrorf(ctx, "[UserFacade] get token from context failed")
		resp.BaseResp = business_error.BaseResp(business_error.TokenParseErr)
		return resp
	}
	uid, err := common_utils.GetUserIDFromToken(token)
	if err != nil {
		klog.CtxErrorf(ctx, "[UserFacade] get user id from token failed: err=%s", err)
		resp.BaseResp = business_error.BaseResp(business_error.TokenParseErr)
		return resp
	}
	if req.NewPassword_ != req.ConfirmPassword {
		resp.BaseResp = business_error.BaseResp(user_error.UserChangeNewPasswordNotEqual)
		return resp
	}
	businessErr := service.UserApp.ChangePassword(assembler.ConvertChangePasswordReqToUserEntity(uid, req), req.NewPassword_)
	if businessErr != nil {
		klog.CtxErrorf(ctx, "[UserFacade] change password failed: err=%s", businessErr)
		resp.BaseResp = business_error.BaseResp(businessErr)
	}
	return resp
}

func (f *User) FunctionAdminList(ctx context.Context, req *user.FunctionAdminListReq) *user.FunctionAdminListResp {
	resp := &user.FunctionAdminListResp{}
	adminList, err := service.UserApp.FunctionAdminList(req.FunctionId)
	if err != nil {
		klog.CtxErrorf(ctx, "[UserFacade] get function admin list failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.AdminList = assembler.ConvertUserEntityListToUserList(adminList)
	return resp
}
