package facade

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/user"
	"gitlab.com/bishe-projects/user_service/src/application/service"
	"gitlab.com/bishe-projects/user_service/src/interface/assembler"
)

var RoleFacade = new(Role)

type Role struct {}

func (f *Role) AllRoleList(ctx context.Context, req *user.AllRoleListReq) *user.AllRoleListResp {
	resp := &user.AllRoleListResp{}
	roleList, err := service.RoleApp.AllRoleList()
	if err != nil {
		klog.CtxErrorf(ctx, "[RoleFacade] get all role list failed: err=%s", err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.RoleList = assembler.ConvertRoleEntityListToRoleList(roleList)
	return resp
}

func (f *Role) UserRoleList(ctx context.Context, req *user.UserRoleListReq) *user.UserRoleListResp {
	resp := &user.UserRoleListResp{}
	roleList, err := service.RoleApp.UserRoleList(req.Uid)
	if err != nil {
		klog.CtxErrorf(ctx, "[RoleFacade] get user role list failed: err=%s", err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.RoleList = assembler.ConvertRoleEntityListToRoleList(roleList)
	return resp
}