package facade

import (
	"context"
	"github.com/bytedance/gopkg/cloud/metainfo"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/common_utils/constant"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/user"
	"gitlab.com/bishe-projects/user_service/src/application/service"
	"gitlab.com/bishe-projects/user_service/src/interface/assembler"
)

var UserRoleFacade = new(UserRole)

type UserRole struct{}

func (f *UserRole) UserAddRole(ctx context.Context, req *user.UserAddRoleReq) *user.UserAddRoleResp {
	resp := &user.UserAddRoleResp{}
	token, ok := metainfo.GetPersistentValue(ctx, "token")
	if !ok {
		klog.CtxErrorf(ctx, "[UserFacade] get token from context failed")
		resp.BaseResp = business_error.BaseResp(business_error.TokenParseErr)
		return resp
	}
	has, err := isAdmin(token)
	if err != nil {
		klog.CtxErrorf(ctx, "[UserRoleFacade] check admin failed: err=%s", err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	if !has {
		klog.CtxErrorf(ctx, "[UserRoleFacade] user have no auth to add role")
		resp.BaseResp = business_error.BaseResp(business_error.NoAuthErr)
		return resp
	}
	err = service.UserRoleApp.UserAddRole(ctx, assembler.ConvertUserAddRoleReqToUserRoleEntity(req))
	if err != nil {
		klog.CtxErrorf(ctx, "[UserRoleFacade] user add role failed: err=%s", err)
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *UserRole) UserRemoveRole(ctx context.Context, req *user.UserRemoveRoleReq) *user.UserRemoveRoleResp {
	resp := &user.UserRemoveRoleResp{}
	token, ok := metainfo.GetPersistentValue(ctx, "token")
	if !ok {
		klog.CtxErrorf(ctx, "[UserFacade] get token from context failed")
		resp.BaseResp = business_error.BaseResp(business_error.TokenParseErr)
		return resp
	}
	has, err := isAdmin(token)
	if err != nil {
		klog.CtxErrorf(ctx, "[UserRoleFacade] check admin failed: err=%s", err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	if !has {
		klog.CtxErrorf(ctx, "[UserRoleFacade] user have no auth to add role")
		resp.BaseResp = business_error.BaseResp(business_error.NoAuthErr)
		return resp
	}
	err = service.UserRoleApp.UserRemoveRole(req.Uid, req.RoleId, req.FunctionId)
	if err != nil {
		klog.CtxErrorf(ctx, "[UserRoleFacade] user remove role failed: err=%s", err)
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *UserRole) HasUserRole(ctx context.Context, req *user.HasUserRoleReq) *user.HasUserRoleResp {
	resp := &user.HasUserRoleResp{}
	has, err := service.UserRoleApp.HasUserRole(req.Uid, req.RoleId, req.FunctionId)
	if err != nil {
		klog.CtxErrorf(ctx, "[UserRoleFacade] has user role failed: err=%s", err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.Has = has
	return resp
}

func isAdmin(token string) (bool, *business_error.BusinessError) {
	uid, err := common_utils.GetUserIDFromToken(token)
	if err != nil {
		return false, business_error.TokenParseErr
	}
	return service.UserRoleApp.HasUserRole(uid, constant.SystemAdminRoleID, nil)
}
