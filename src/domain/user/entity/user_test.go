package entity

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/user_error"
	"reflect"
	"testing"
)

func TestUser_CreateUser(t *testing.T) {
	type fields struct {
		ID       int64
		Name     string
		Password string
		Roles    RoleList
	}
	tests := []struct {
		name   string
		fields fields
		want   *business_error.BusinessError
	}{
		{name: "normal", fields: fields{Name: "test", Password: "123123"}, want: nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &User{
				ID:       tt.fields.ID,
				Name:     tt.fields.Name,
				Password: tt.fields.Password,
				Roles:    tt.fields.Roles,
			}
			if got := u.CreateUser(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateUser() = %v, want %v", got, tt.want)
			}
			t.Logf("user=%+v", u)
		})
	}
}

func TestUser_GetUserByID(t *testing.T) {
	type fields struct {
		ID       int64
		Name     string
		Password string
		Roles    RoleList
	}
	type args struct {
		userID int64
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *business_error.BusinessError
	}{
		{name: "found", fields: fields{}, args: args{2}, want: nil},
		{name: "found & no user role", fields: fields{}, args: args{4}, want: nil},
		{name: "not found", fields: fields{}, args: args{-1}, want: user_error.GetUserByIDErr},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &User{
				ID:       tt.fields.ID,
				Name:     tt.fields.Name,
				Password: tt.fields.Password,
				Roles:    tt.fields.Roles,
			}
			if got := u.GetUserByID(tt.args.userID); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetUserByID() = %v, want %v", got, tt.want)
			}
			t.Logf("user=%+v", u)
		})
	}
}

func TestUser_GetUserByUsername(t *testing.T) {
	type fields struct {
		ID       int64
		Name     string
		Password string
		Roles    RoleList
	}
	type args struct {
		username string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *business_error.BusinessError
	}{
		{name: "found", fields: fields{}, args: args{"user0"}, want: nil},
		{name: "not found", fields: fields{}, args: args{"-1"}, want: business_error.DataNotFoundErr},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &User{
				ID:       tt.fields.ID,
				Name:     tt.fields.Name,
				Password: tt.fields.Password,
				Roles:    tt.fields.Roles,
			}
			got := u.GetUserByUsername(tt.args.username)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetUserByUsername() = %v, want %v", got, tt.want)
			}
			t.Logf("user=%+v err=%s", u, got)
		})
	}
}

func TestUser_checkPasswordRules(t *testing.T) {
	type fields struct {
		ID       int64
		Name     string
		Password string
		Roles    RoleList
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{name: "valid", fields: fields{Password: "12345678"}, want: true},
		{name: "too short", fields: fields{Password: "123"}, want: false},
		{name: "too long", fields: fields{Password: "123456789012345678901"}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &User{
				ID:       tt.fields.ID,
				Name:     tt.fields.Name,
				Password: tt.fields.Password,
				Roles:    tt.fields.Roles,
			}
			if got := u.CheckPasswordRules(); got != tt.want {
				t.Errorf("CheckPasswordRules() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserList_FunctionAdminList(t *testing.T) {
	type args struct {
		functionId int64
	}
	tests := []struct {
		name string
		ul   UserList
		args args
		want *business_error.BusinessError
	}{
		{name: "found", ul: UserList{}, args: args{1}, want: nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ul.FunctionAdminList(tt.args.functionId); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FunctionAdminList() = %v, want %v", got, tt.want)
			}
			t.Logf("adminList=%+v", tt.ul)
		})
	}
}
