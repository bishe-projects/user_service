package entity

import (
	"fmt"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/repo/po"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/user_error"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/util"
	"gorm.io/gorm"
)

const (
	PasswordMinLen = 6
	PasswordMaxLen = 20
)

type User struct {
	ID       int64
	Name     string
	Password string
	Roles    RoleList
}

func NewUser(name string, password string) *User {
	return &User{
		Name:     name,
		Password: password,
	}
}

func (u *User) CreateUser() *business_error.BusinessError {
	if !u.CheckPasswordRules() {
		return user_error.UserPasswordNotSatisfied
	}
	u.encodedPassword()
	userPO := u.convertUserToUserPO()
	err := repo.UserMySQLRepo.UserDao.CreateUser(userPO)
	if err != nil {
		klog.Errorf("[UserAggregate] create user failed: err=%s", err)
		return user_error.CreateUserErr
	}
	u.fillUserFromUserPO(userPO)
	return nil
}

func (u *User) UpdateUser(values map[string]interface{}) *business_error.BusinessError {
	err := repo.UserMySQLRepo.UserDao.UpdateUser(u.convertUserToUserPO(), values)
	if err != nil {
		klog.Errorf("[UserAggregate] user update failed: err=%s", err)
		return user_error.UserUpdateErr
	}
	return nil
}

func (u *User) GetUserByID(userID int64) *business_error.BusinessError {
	userPO, err := repo.UserMySQLRepo.UserDao.GetUserByID(userID)
	if err == gorm.ErrRecordNotFound {
		return business_error.DataNotFoundErr
	}
	if err != nil {
		klog.Errorf("[UserAggregate] get user by id failed: err=%s", err)
		return user_error.GetUserByIDErr
	}
	u.fillUserFromUserPO(userPO)
	userRoleListErr := u.Roles.UserRoleList(u.ID)
	if userRoleListErr != nil {
		klog.Errorf("[UserAggregate] get user role list failed: err=%s", err)
		return user_error.GetUserRoleListErr
	}
	return nil
}

func (u *User) GetUserByUsername(username string) *business_error.BusinessError {
	userPO, err := repo.UserMySQLRepo.UserDao.GetUserByUsername(username)
	if err == gorm.ErrRecordNotFound {
		return business_error.DataNotFoundErr
	}
	if err != nil {
		klog.Errorf("[UserAggregate] get user by username failed: err=%s", err)
		return user_error.GetUserByNameErr
	}
	u.fillUserFromUserPO(userPO)
	userRoleListErr := u.Roles.UserRoleList(u.ID)
	if userRoleListErr != nil {
		klog.Errorf("[UserAggregate] get user role list failed: err=%s", err)
		return user_error.GetUserRoleListErr
	}
	return nil
}

func (u *User) encodedPassword() {
	u.Password = util.GetMD5Hash(u.Password)
}

func (u *User) CheckPasswordRules() bool {
	return len(u.Password) >= PasswordMinLen && len(u.Password) <= PasswordMaxLen
}

func (u *User) IsInputPasswordValid(inputPassword string) bool {
	return util.GetMD5Hash(inputPassword) == u.Password
}

type UserList []*User

func (ul *UserList) FunctionAdminList(functionId int64) *business_error.BusinessError {
	userPOList, err := repo.UserMySQLRepo.UserDao.FunctionAdminList(functionId)
	if err != nil {
		klog.Errorf("[UserAggregate] get function admin list failed: functionId=%d err=%s", functionId, err)
		return user_error.FunctionAdminListErr
	}
	ul.fillUserListFromUserPOList(userPOList)
	return nil
}

// converter
func (u *User) convertUserToUserPO() *po.User {
	return &po.User{
		ID:       u.ID,
		Name:     u.Name,
		Password: u.Password,
	}
}

func (u *User) fillUserFromUserPO(userPO *po.User) {
	u.ID = userPO.ID
	u.Name = userPO.Name
	u.Password = userPO.Password
}

func (ul *UserList) fillUserListFromUserPOList(userPOList []*po.User) {
	*ul = make([]*User, 0, len(userPOList))
	for _, userPO := range userPOList {
		user := new(User)
		user.fillUserFromUserPO(userPO)
		*ul = append(*ul, user)
	}
}

func (u *User) String() string {
	return fmt.Sprintf("{id=%d, name=%s}", u.ID, u.Name)
}
