package entity

import (
	"fmt"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/repo/po"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/user_error"
)

type Role struct {
	ID                 int64
	Name               string
	ManageFunctionId   *int64
	ManageFunctionName *string
}

type RoleList []*Role

func (rl *RoleList) AllRoleList() *business_error.BusinessError {
	rolePOList, err := repo.RoleRepo.RoleDao.AllRoleList()
	if err != nil {
		klog.Errorf("[RoleEntity] get all role list failed: err=%s", err)
		return user_error.GetAllRoleListErr
	}
	rl.fillRoleListFromRolePOList(rolePOList)
	return nil
}

func (rl *RoleList) UserRoleList(userID int64) *business_error.BusinessError {
	roleResultPOList, err := repo.RoleRepo.RoleDao.UserRoleList(userID)
	if err != nil {
		klog.Errorf("[RoleEntity] get user role list failed: err=%s", err)
		return user_error.GetUserRoleListErr
	}
	rl.fillRoleListFromRoleResultPOList(roleResultPOList)
	return nil
}

func NewRole(id int64, name string) *Role {
	return &Role{
		ID:   id,
		Name: name,
	}
}

func NewRoleWithFunction(id, manageFunctionId int64, name, manageFunctionName string) *Role {
	return &Role{
		ID:                 id,
		Name:               name,
		ManageFunctionId:   &manageFunctionId,
		ManageFunctionName: &manageFunctionName,
	}
}

// converter
func (r *Role) fillRoleFromRolePO(rolePO *po.Role) {
	r.ID = rolePO.ID
	r.Name = rolePO.Name
}

func (rl *RoleList) fillRoleListFromRolePOList(rolePOList []*po.Role) {
	*rl = make([]*Role, 0, len(rolePOList))
	for _, rolePO := range rolePOList {
		role := new(Role)
		role.fillRoleFromRolePO(rolePO)
		*rl = append(*rl, role)
	}
}

func (r *Role) fillRoleFromRoleResultPO(roleResultPO *po.RoleResult) {
	r.ID = roleResultPO.ID
	r.Name = roleResultPO.Name
	r.ManageFunctionId = roleResultPO.ManageFunctionId
	r.ManageFunctionName = roleResultPO.ManageFunctionName
}

func (rl *RoleList) fillRoleListFromRoleResultPOList(roleResultPOList []*po.RoleResult) {
	*rl = make([]*Role, 0, len(roleResultPOList))
	for _, roleResultPO := range roleResultPOList {
		role := new(Role)
		role.fillRoleFromRoleResultPO(roleResultPO)
		*rl = append(*rl, role)
	}
}

func (r *Role) String() string {
	return fmt.Sprintf("id=%d, name=%s, manageFunctionId=%+v, manageFunctionName=%+v", r.ID, r.Name, r.ManageFunctionId, r.ManageFunctionName)
}
