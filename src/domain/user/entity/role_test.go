package entity

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"reflect"
	"testing"
)

func TestRoleList_AllRoleList(t *testing.T) {
	tests := []struct {
		name string
		rl   RoleList
		want *business_error.BusinessError
	}{
		{name: "normal", rl: RoleList{}, want: nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.rl.AllRoleList(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AllRoleList() = %v, want %v", got, tt.want)
			}
			t.Logf("AllRoleList=%+v", tt.rl)
		})
	}
}

func TestRoleList_UserRoleList(t *testing.T) {
	type args struct {
		userID int64
	}
	tests := []struct {
		name string
		rl   RoleList
		args args
		want *business_error.BusinessError
	}{
		{name: "found", rl: RoleList{}, args: args{2}, want: nil},
		{name: "not found", rl: RoleList{}, args: args{-1}, want: nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.rl.UserRoleList(tt.args.userID); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UserRoleList() = %v, want %v", got, tt.want)
			}
			t.Logf("UserRoleList=%+v", tt.rl)
		})
	}
}
