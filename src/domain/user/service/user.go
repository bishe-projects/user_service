package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/user_service/src/domain/user/entity"
)

var UserDomain = new(User)

type User struct{}

func (d *User) GetUserByID(userID int64) (*entity.User, *business_error.BusinessError) {
	user := new(entity.User)
	err := user.GetUserByID(userID)
	if err == business_error.DataNotFoundErr {
		return nil, nil
	}
	return user, err
}

func (d *User) GetUserByUsername(username string) (*entity.User, *business_error.BusinessError) {
	user := new(entity.User)
	err := user.GetUserByUsername(username)
	if err == business_error.DataNotFoundErr {
		return nil, nil
	}
	return user, err
}

func (d *User) CreateUser(registerUser *entity.User) *business_error.BusinessError {
	return registerUser.CreateUser()
}

func (d *User) UpdateUser(user *entity.User, values map[string]interface{}) *business_error.BusinessError {
	return user.UpdateUser(values)
}

func (d *User) FunctionAdminList(functionId int64) ([]*entity.User, *business_error.BusinessError) {
	adminList := new(entity.UserList)
	err := adminList.FunctionAdminList(functionId)
	return *adminList, err
}
