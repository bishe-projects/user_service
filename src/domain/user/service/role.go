package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/user_service/src/domain/user/entity"
)

var RoleDomain = new(Role)

type Role struct {}

func (d *Role) AllRoleList() ([]*entity.Role, *business_error.BusinessError) {
	roleList := new(entity.RoleList)
	err := roleList.AllRoleList()
	return *roleList, err
}

func (d *Role) UserRoleList(userID int64) ([]*entity.Role, *business_error.BusinessError) {
	roleList := new(entity.RoleList)
	err := roleList.UserRoleList(userID)
	return *roleList, err
}