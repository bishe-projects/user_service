package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/user_service/src/domain/user_role/entity"
)

var UserRoleDomain = new(UserRole)

type UserRole struct{}

func (d *UserRole) GetUserRole(uid, roleID int64, functionID *int64) (*entity.UserRole, *business_error.BusinessError) {
	userRole := new(entity.UserRole)
	err := userRole.GetUserRole(uid, roleID, functionID)
	if err == business_error.DataNotFoundErr {
		return nil, nil
	}
	return userRole, err
}

func (d *UserRole) UserAddRole(userRole *entity.UserRole) *business_error.BusinessError {
	return userRole.UserAddRole()
}

func (d *UserRole) UserRemoveRole(userRole *entity.UserRole) *business_error.BusinessError {
	return userRole.UserRemoveRole()
}
