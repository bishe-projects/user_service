package service

import (
	"github.com/apache/thrift/lib/go/thrift"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/user_service/src/domain/user_role/entity"
	"reflect"
	"testing"
)

func TestUserRole_GetUserRole(t *testing.T) {
	type args struct {
		uid        int64
		roleID     int64
		functionID *int64
	}
	tests := []struct {
		name  string
		args  args
		want  *entity.UserRole
		want1 *business_error.BusinessError
	}{
		{name: "not found", args: args{-1, 1, thrift.Int64Ptr(1)}, want: nil, want1: nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &UserRole{}
			got, got1 := d.GetUserRole(tt.args.uid, tt.args.roleID, tt.args.functionID)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetUserRole() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("GetUserRole() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
