package entity

import (
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/repo/po"
	"gitlab.com/bishe-projects/user_service/src/infrastructure/user_error"
	"gorm.io/gorm"
)

type UserRole struct {
	ID         int64
	Uid        int64
	RoleId     int64
	FunctionId int64
}

func (ur *UserRole) GetUserRole(uid, roleID int64, functionID *int64) *business_error.BusinessError {
	userRolePO, err := repo.UserRoleRepo.UserRoleDao.GetUserRole(uid, roleID, functionID)
	if err == gorm.ErrRecordNotFound {
		return business_error.DataNotFoundErr
	}
	if err != nil {
		klog.Errorf("[UserRoleAggregate] get user role failed: err=%s", err)
		return user_error.GetUserRoleErr
	}
	ur.fillUserRoleFromUserRolePO(userRolePO)
	return nil
}

func (ur *UserRole) UserAddRole() *business_error.BusinessError {
	userRolePO := ur.convertUserRoleToUserRolePO()
	err := repo.UserRoleRepo.UserRoleDao.UserAddRole(userRolePO)
	if err == error_enum.ErrDuplicateEntry {
		return business_error.DataExistsErr
	}
	if err != nil {
		klog.Errorf("[UserRoleAggregate] create user role failed: err=%s", err)
		return user_error.UserAddRoleErr
	}
	ur.fillUserRoleFromUserRolePO(userRolePO)
	return nil
}

func (ur *UserRole) UserRemoveRole() *business_error.BusinessError {
	userRolePO := ur.convertUserRoleToUserRolePO()
	err := repo.UserRoleRepo.UserRoleDao.UserRemoveRole(userRolePO)
	if err != nil {
		klog.Errorf("[UserRoleAggregate] remove user role failed: err=%s", err)
		return user_error.UserRemoveRoleErr
	}
	return nil
}

// converter
func (ur *UserRole) convertUserRoleToUserRolePO() *po.UserRole {
	return &po.UserRole{
		ID:         ur.ID,
		Uid:        ur.Uid,
		RoleId:     ur.RoleId,
		FunctionId: ur.FunctionId,
	}
}

func (ur *UserRole) fillUserRoleFromUserRolePO(userRolePO *po.UserRole) {
	ur.ID = userRolePO.ID
	ur.Uid = userRolePO.Uid
	ur.RoleId = userRolePO.RoleId
	ur.FunctionId = userRolePO.FunctionId
}
