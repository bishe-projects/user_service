package main

import (
	"github.com/cloudwego/kitex/server"
	user "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/user/userservice"
	"log"
	"net"
)

func main() {
	addr, _ := net.ResolveTCPAddr("tcp", "127.0.0.1:8881")
	svr := user.NewServer(new(UserServiceImpl), server.WithServiceAddr(addr))

	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}
